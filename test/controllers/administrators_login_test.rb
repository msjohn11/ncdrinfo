require 'test_helper'

class AdministratorsLoginTest < ActionDispatch::IntegrationTest

  def setup
    @administrator = administrators(:tester)
  end

test "signin with invalid information" do
    get signin_path
    assert_template 'sessions/new'
    test_session = { session: { email: "A", password: "B" } }
    post signin_path, test_session
    assert_template 'sessions/new'
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end
  
  
  test "login with valid information" do
    get signin_path
    post signin_path, { session: { email: @administrator.email, password: 'password' } }
    assert_redirected_to insurance_path
    follow_redirect!
    assert_template 'administrators/companies'
  end
end