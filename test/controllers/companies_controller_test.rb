require 'test_helper'

class CompaniesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    #main page loads
    assert_select "#jumbo-title", "NC Diabetes Reference for Insurance Formularies"
    #medicare partials load
    assert_select "#MedicarePartD", true
    assert_select "#MedicareAdvantage", true
    #detail page should not load
    assert_select "#company-url", ""
  end
  

end
