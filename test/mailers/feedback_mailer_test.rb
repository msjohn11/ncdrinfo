require 'test_helper'

class FeedbackMailerTest < ActionMailer::TestCase
  test "feedback" do
    # Create the email and store it for further assertions
    args = {name: 'Scott', email: 'me@example.com', subject: 'test', message: 'Test message'}
    email = FeedbackMailer.feedback_email(args)
 
    # Send the email, then test that it got queued
    assert_emails 1 do
      email.deliver_now
    end
 
    # Test the body of the sent email contains what we expect it to
    assert_equal ['me@example.com'], email.from
    assert_equal ['ncdrinfomation@gmail.com'], email.to
    assert_equal read_fixture('feedback.txt').join, email.body.to_s
  end
end
