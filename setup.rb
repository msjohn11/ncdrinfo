require 'active_record'
require './app/models/administrator.rb'
require 'io/console'

VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

#from http://stackoverflow.com/questions/2338889/how-to-hide-password-input-from-terminal-in-ruby-script
userid = `read -p "Enter administrator email: " uid; echo $uid`.chomp
passwd = `stty -echo; read -p "Password: " password; echo $password`.chomp
`stty echo`
password = Administrator.digest(passwd)
puts "Configuring adminstrator account..."

if userid.match(VALID_EMAIL_REGEX) == nil
    abort("Not a valid email address")
end

open('db/seeds.rb', 'a') { |f|
  f.puts "\nAdministrator.create!([{email: \"#{userid}\", password_digest: \"#{password}\"}])"
}
`git commit -am "Adding admin login"`
`git push heroku master`
`heroku restart; heroku pg:reset DATABASE --confirm pacific-ocean-74371; heroku run rake db:migrate db:seed`
puts "Configuration complete"