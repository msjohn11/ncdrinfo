class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string 'name'
      t.string 'url'
      t.string 'plan_year'
      t.column :co_type, :integer, default: 0
      t.timestamps
    end
  end
end
