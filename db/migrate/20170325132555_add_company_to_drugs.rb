class AddCompanyToDrugs < ActiveRecord::Migration
  def change
    add_reference :drugs, :company, index: true, foreign_key: true
  end
end
