class CreateDrugs < ActiveRecord::Migration
  def change
    create_table :drugs do |t|
      t.string 'name'
      t.string 'drug_class'
      t.string 'tier'
      t.string 'comments'
      t.string 'dose'
      t.string 'price'
      t.string 'quantity'
      t.timestamps
    end
  end
end
