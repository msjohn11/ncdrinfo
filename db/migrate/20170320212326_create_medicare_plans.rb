class CreateMedicarePlans < ActiveRecord::Migration
  def change
    create_table :medicare_plans do |t|
      t.boolean :is_advantage
      t.string :name
      t.string :url
      t.string :county
      t.datetime :plan_year
      t.timestamps
    end
  end
end
