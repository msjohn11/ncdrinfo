Rails.application.routes.draw do
  #routes for the main page
  resources :companies
  root 'companies#index'
  get '/companies' => 'companies#index'
  get '/companies/:id/formulary/' => 'companies#formulary', as: :formulary
  get '/drugs/:id/get_dollar_list' => 'drugs#get_dollar_list', as: :dollar_list
  
  #routes used for signing in and out
  get '/signin'  => 'sessions#new', as: :signin
  post '/signin' => 'sessions#create'
  delete '/logout' => 'sessions#destroy'
  
  #route to about page
  get '/about' => 'static_pages#about'
  
  #route to contact page
  get '/contact' => 'contact#contact'
  post '/contact' => 'contact#feedback'
  
  #routes used for data administration
  get '/insurance' => 'administrators#companies', as: :insurance
  get '/pharmacies' => 'administrators#companies', as: :pharmacies
  get '/administrators/:id' => 'administrators#show', as: :company_plan
  
  post '/companies/:id' => 'companies#add_update'

  post '/drugs' => 'drugs#create'
  post '/drugs/:id' => 'drugs#update'

  delete '/drugs/:id' => 'drugs#destroy'
  delete '/companies/:id' => 'companies#destroy'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
