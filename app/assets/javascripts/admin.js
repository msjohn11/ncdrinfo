// Defining the columns of the company table.
var metadata = [];
metadata.push({ name: "name", label: "Drug Name", datatype: "string", editable: true});
metadata.push({ name: "drug_class", label:"Drug Class", datatype: "string", editable: true});
metadata.push({ name: "tier", label: "Tier", datatype: "string", editable: true});
metadata.push({ name: "dose", label:"Dose", datatype: "string", editable: true});
metadata.push({ name: "price", label: "Price", datatype: "string", editable: true});
metadata.push({ name: "quantity", label: "Quantity", datatype: "string", editable: true});
metadata.push({ name: "action", label: "Action", datatype: "html", editable: false });

//create the table object.
var editableGrid = new EditableGrid("CompanyDrugList");

//empty array to hold table data.
var tableData = [];

//this is the html that givs us editing buttons in the drug table.
var actionString =
    '<div class="btn-group-xs" role="group">' +
        '<button type="button" class ="btn btn-success" data-toggle="tooltip" data-placement="top" title data-original-title="Save item" onclick="saveLine($(this).closest(\'tr\'))"><i class="glyphicon glyphicon-thumbs-up" aria-hidden=true></i></button>' +
        '<button type="button" class ="btn btn-warning" data-toggle="tooltip" data-placement="top" title data-original-title="Cancel changes" onclick="discardChanges($(this).closest(\'tr\'))"><i class="glyphicon glyphicon-thumbs-down" aria-hidden=true></i></button>' +
        '<button type="button" class ="btn btn-danger data-toggle="tooltip" data-placement="top" title data-original-title="Delete item" onclick="deleteLine($(this).closest(\'tr\'))"><i class="glyphicon glyphicon-trash" aria-hidden=true></i></button>' +
    '</div>';

var current_company_id = "-1";
var company_type = "";

function appendOnAdminClick() {
    $('#admin-dropdown a').click(function (e) {
        var name = this.innerHTML;
        if (name != "Add Company") {
            $('#admin-button').text(this.innerHTML);
            $('#companyURL').empty();
            $('#admin-dropdown').empty();
            tableData = [];
            //get the company info and drug list via AJAX
            $.ajax({
                type: 'GET',
                url: '/administrators/' + this.innerHTML.trim(),
                success: function(data) {
                    current_company_id = data.company.id;
                    company_type = data.company.co_type;
                    
                    $.each(data.formulary, function (i, item) {
                        item.action = actionString;
                        tableData.push({ id: 'E' + item.id, values: item });
                    });
                    editableGrid.load({ "metadata": metadata, "data": tableData });
                    editableGrid.renderGrid("tablecontent", "testgrid");
                    $('#companyURL').val(data.company.url);
                    $('#companyURL').removeAttr('placeholder');
                    addCompanyEdit();
                    $('#admin-name-div input').val(data.company.name);
                    setColumns();
                }
            });
        } else if (name == "Add Company"){
            $('#companyURL').val('');
            $('#companyURL').attr('placeholder', "Formulary URL");
            $('#admin-button').text('');
            $('#admin-dropdown').empty();
            addForm('#admin-button');
        }

        $('#admin-add-row').show();
        addRowClick();
    });
}

//show the right columns depending on pharmacy or company
function setColumns(){
    if(company_type == "pharmacy"){
        $(".editablegrid-tier").hide();
    } else {
        $(".editablegrid-dose").hide();
        $(".editablegrid-price").hide();
        $(".editablegrid-quantity").hide();
    }
}

//add click action to the "add row" link.
function addRowClick(){
$('#admin-add-row a').click(function(e){
        e.preventDefault();
        var blank_data = {name:'', drug_class:'', tier:'', dose:'', price:'', quantity:'', action: actionString};
        var id = 'N' + Date.now();
        tableData.push({ id: id, values: blank_data });
        editableGrid.append('N' + Date.now(), blank_data, true, true);
        setColumns();
    });
}

function addCompanyEdit(){
    $('#admin-name-div').append(
        '<form id="edit-form"><div class="input-group" id="new_name_input">' +
        '<input type="text" class="form-control" id="new_name">' +
        '<div class="input-group-btn">' +
        '<button class="btn btn-default" type="submit" id="save-all"><i class="glyphicon glyphicon-floppy-save"></i></button>' +
        '<button class="btn btn-default" type="submit" id="delete-all"><i class="glyphicon glyphicon-trash"></i></button>' +
        '<button class="btn btn-default" type="submit" id="edit-complete"><i class="glyphicon glyphicon-lock"></i></button></div></div></form>'
    );
    $('#save-all').click(function(e){
        e.preventDefault();
        saveAll();
    });

    $('#delete-all').click(function(e){
        e.preventDefault();
        deleteCompany();
    });

    $('#edit-complete').click(function (e) {
        reloadWindow();
    });
}

function saveAll() {
    if (window.confirm('This will save all changes to this record.')) {
        $.ajax({
          type: "POST",
          url: "/companies/" + current_company_id,
          data: { company: { name: $("#new_name").val(), url: $("#companyURL").val() } },
          success:function(data) {
            current_company_id = data.company.id
            $('.testgrid > tbody > tr').each(function(){
                saveLine($(this));
            });
            return updateSuccess(data);
          },
          error: function(data) {
              return updateError(data);
          }
        });
    } else {
        alert('changes not saved');
    }
}

function deleteCompany() {
    if (window.confirm("This will remove the company and it's formulary from from the database.")) {
        $.ajax({
            type: "DELETE",
            url: '/companies/' + current_company_id,
            success:function(data) {
                updateSuccess(data);
                reloadWindow();
                return false;
            },
            error: function(data) {
                return updateError(data);
            }
        });
    }
}

function reloadWindow() {
    setTimeout(function () {
        window.location.reload();
    }, 100);
}

function addForm(divId){
    addCompanyEdit();
    $('#admin-name-div input').attr('placeholder', 'New Company');
    var blank_data = {name:'', drug_class:'', tier:'', dose:'', price:'', quantity:'', action: actionString};
    tableData = [];
    tableData.push({ id: 'N' + Date.now(), values: blank_data });
    editableGrid = new EditableGrid("CompanyDrugList");
    editableGrid.load({ "metadata": metadata, "data": tableData });
    editableGrid.renderGrid("tablecontent", "testgrid");
    setColumns();
}

//existing drugs have an id with E for existing. New drugs have an id that starts with N.
function saveLine(rowId){
    //determine new vs update
    if(/E\d+/.test(rowId.attr('id'))){
        updateDrug(rowId);
    } else if(/N\d+/.test(rowId.attr('id'))){
        createDrug(rowId)
    }
}

var dismiss = $('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>');

function updateDrug(rowId) {
    var drugId = rowId.attr('id').match(/\d+/)[0]; //this assumes way too much
    var gridRowIndex = editableGrid.getRowIndex('E' + drugId);
    var gridRow = editableGrid.getRowValues(gridRowIndex);
    var index = tableData.findIndex(function(x) {
        return x.id == 'E' + drugId;
    });
    var changes = {
        company_id: current_company_id,
        name: gridRow.name,
        drug_class: gridRow.drug_class,
        tier: gridRow.tier,
        dose: gridRow.dose,
        price: gridRow.price,
        quantity: gridRow.quantity
    };
    $.ajax({
        type: "POST",
        url: "/drugs/" + drugId,
        data: changes,
        success: function(data) {
            tableData[index].values = gridRow;
            return updateSuccess(data);
        },
        error: function(data) {
            return updateError(data);
        }
    });
}

function createDrug(rowId) {
    var drugId = rowId.attr('id').match(/\d+/)[0]; //this assumes way too much
    var gridRowIndex = editableGrid.getRowIndex('N' + drugId)
    var gridRow = editableGrid.getRowValues(gridRowIndex);
    var data = {
        company_id: current_company_id,
        name: gridRow.name,
        drug_class: gridRow.drug_class,
        tier: gridRow.tier,
        dose: gridRow.dose,
        price: gridRow.price,
        quantity: gridRow.quantity
    };
    $.ajax({
        type: "POST",
        url: "/drugs",
        data: data,
        success: function(data) {
            var drug = data.drug;
            //remove temporary row
            editableGrid.remove(gridRowIndex);
            tableData.splice(gridRowIndex, 1);
            //replace with new data
            var values = {
                name: drug.name,
                drug_class: drug.drug_class,
                tier: drug.tier,
                dose: drug.dose,
                price: drug.price,
                quantity: drug.quantity,
                action: actionString
            };
            tableData.push({
                id: 'E' + drug.id,
                values: values
            });
            editableGrid.append('E' + drug.id, values, true, true);
            return updateSuccess(data);
        },
        error: function(data) {
            return updateError(data);
        }
    });
}

function deleteLine(rowId){
    if (window.confirm("Are you sure you want to delete this item?")) {
        var drugId = rowId.attr('id').match(/\w\d+/)[0];
        var gridRowIndex = editableGrid.getRowIndex(drugId)
        $.ajax({
            type: "DELETE",
            url: "/drugs/" +drugId.match(/\d+/)[0],
            data: {company_id: current_company_id},
            success:function(data) {
                editableGrid.remove(gridRowIndex);
                tableData.splice(gridRowIndex,1);
                setColumns();
                return updateSuccess(data);
            },
            error: function(data) {
                return updateError(data);
            }
        });
    }
}

function updateSuccess(data){
    $('#edit').empty();
    $('#edit').append('<div class="alert alert-success alert-dismissable">' + data.message + '</div>');
    $('.alert').append(dismiss);
    return false;
}

function updateError(data){
    $('#edit').empty();
    console.log(data);
    $('#edit').append('<div class="alert alert-warning alert-dismissable">' + data.responseText + '</div>');
    $('.alert').append(dismiss);
    return false;
}

function discardChanges(rowId){
    var tableRowId = rowId.attr('id').match(/\w\d+/)[0];
    var arr_index = tableData.findIndex(function(x) {
       return x.id == tableRowId;
    });
    var gridRowIndex = editableGrid.getRowIndex(tableRowId);
    var values = tableData[arr_index].values;
    editableGrid.setValueAt(gridRowIndex, 0, values.name, true);
    editableGrid.setValueAt(gridRowIndex, 1, values.drug_class, true);
    editableGrid.setValueAt(gridRowIndex, 2, values.tier, true);
    editableGrid.setValueAt(gridRowIndex, 3, values.dose, true);
    editableGrid.setValueAt(gridRowIndex, 4, values.price, true);
    editableGrid.setValueAt(gridRowIndex, 5, values.quantity, true);
}

var ready_admin;

ready_admin = function(){
    appendOnAdminClick();
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
};

$(document).ready(ready_admin);
$(document).on('page:change', ready_admin);