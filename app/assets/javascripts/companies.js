var ready;

ready = function(){
    addMedicare();
    addMouseEnter();
    slideUp();
    //attach an action to the medicare modals for when they close.
    $('.modal').on('hidden.bs.modal', function() {
        window.location.replace("/companies");
    });
    //refresh the page on button click.
    $('#refresh').click(function() {
        window.location.replace("/companies");
    });
};

$(document).ready(ready);
$(document).on('page:load', ready);

//add click action to medicare links so modals will show.
function addMedicare(){
    var plans = $('#insurance-dropdown a').filter(function () {
        return /Medicare/.test($(this).text().trim());
    });
    $.each(plans, function(){
        $(this).click(function(e){
            e.preventDefault();
            var div = $(this).text().replace(/\W*/g, "");
            $('#' + div).modal('show');
            //set button to company name
            $('#company-button').text($(this).innerHTML);
            //clear out the old data
            clearData();
        });
    });
}

function clearData(){
    $('#company-url').empty();
    $('#drug-list').empty();
    $('#drug-table').empty();
}

function getDollarDrugTable(target_drug) {
    var drugName = target_drug.replace(' ', '');
    $.ajax({
        type: 'GET',
        url: '/drugs/' + encodeURI(target_drug.replace(/\//g," ")) + '/get_dollar_list',
        success: function(data) {
            $('#drug-table').html(data.list);
        }
    });
}

function slideUp(){
    $('#page-info').slideUp();
    $('#slider-icon').empty();
    $('#slider-icon').append('<center><span class="glyphicon glyphicon-chevron-down" aria-hidden="true" onclick="slideDown()"></span></center>');
}

function slideDown(){
    $('#page-info').slideDown();
    $('#slider-icon').empty();
    $('#slider-icon').append('<center><span class="glyphicon glyphicon-chevron-up" aria-hidden="true" onclick="slideUp()"></span></center>');
}

//for any item in the drug list, show discont drugs when hovered over
function addMouseEnter(){
    $('.drug-item').each(function(){
        $(this).mouseenter(mouseEnter);
    });
}

// Show the $4 generics if a cheap drug is moused over.
function mouseEnter(e) {
    var name = $(this).text().trim();
    if($(this).filter(".formulary").length == 0){
        getDollarDrugTable(name);
    }
}
