module CompaniesHelper
    def dollar_drugs_in_class(drug_class)
        discount_drugs = Hash.new
        @pharmacy_drugs.each do |drug|
            added_drug_list = []
            if (((drug.drug_class == "Combination" && drug_class.start_with?("Bi")) ||
                    drug_class.downcase.start_with?(drug.drug_class.downcase) ||
                    (drug_class.start_with?("Sulfonyl") && drug_class.downcase.match(drug.drug_class.downcase))) &&
                    !added_drug_list.include?(drug.name))
                added_drug_list.push(drug.name);
                drugId = drug.name.gsub(/\W+/, '')
                discount_drugs[drugId] = drug.name
            end
        end
        return discount_drugs
    end
    
    def common_drug(test_drug)
        @pharmacy_drugs.each do |drug|
            if drug.name =~ /#{test_drug.name}/i || test_drug.name =~ /#{drug.name}/i
                return "both"
            end
        end
        return "formulary"
    end
    
      def get_dollar_list
        
        #split up the search term into words.
        search_drugs = params[:id].scan(/\w{4,}/)
        
        #this brings back ALL pharmacies and ALL drugs. It sucks.
        pharmacies = Company.where(co_type: Company.co_types[:pharmacy]).order(:name)
        pharmacy_ids = Company.select("id").where(co_type: Company.co_types[:pharmacy]).map{|pharmacy| pharmacy.id}
        drugs =  Drug.where("company_id IN (?)", pharmacy_ids) 
    
        drug_map = Hash.new([])
        
        #this weeds the list down to those that match our search term. All because SQLITE cant handle "LIKE" searches on an array.
        #it was this or do multiple SQL queries. This is faster on a small dataset like this.
        pharmacies.each do |pharmacy|
          drug_map[{:name => pharmacy.name, :url => pharmacy.url}] = []
          drugs.each do |drug|
            if pharmacy.id == drug.company_id && search_drugs.any?{ |w| drug.name =~ /#{w}/i }
              drug_map[{:name => pharmacy.name, :url => pharmacy.url}] += 
                  [{:name => drug.name, :price => drug.price, :dose => drug.dose, :quantity => drug.quantity}]
            end
          end
        end
        drug_map[{name: "test", url: "test_url"}] = [{name: "every", dose: "good", price: "boy", quantity: "fine"}]
        return drug_map
      end
   end

