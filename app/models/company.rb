class Company < ActiveRecord::Base
    has_many :drugs, dependent: :destroy
    enum co_type: [ :insurance, :pharmacy ]
    validates :name, presence: true
    validates :co_type,
        :inclusion  => { :in => co_types,
        :message    => "%{value} is not a valid company type" }
end