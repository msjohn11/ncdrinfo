class MedicarePlan < ActiveRecord::Base
    validates :name, presence: true
    validate :advantage_plan_county
    private
    def advantage_plan_county
        if is_advantage && county.blank?
            errors.add(:base, "Advantage plans must be assigned a county")
        end
    end
end
