class FeedbackMailer < ApplicationMailer
    default from: 'ncdrinfomation@gmail.com'
 
    def feedback_email(args)
        message = "From: #{args[:name]}\nRe: #{args[:subject]}\n#{args[:message]}"
        mail(to: "ncdrinfomation@gmail.com", from: args[:email], subject: args[:subject], body: message)
    end
end
