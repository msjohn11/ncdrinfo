class ApplicationMailer < ActionMailer::Base
  default from: "ncdrinformation@gmail.com"
  layout 'mailer'
end
