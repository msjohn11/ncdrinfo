class DrugsController < ApplicationController
  before_action :set_drug, only: [:show, :edit, :update, :destroy]

  # GET /drugs
  # GET /drugs.json
  def index
    @drugs = Drug.all
  end

  # POST /drugs
  # POST /drugs.json
  def create
    @drug = Drug.new(drug_params)
    respond_to do |format|
      if @drug.save
        msg = { :status => "ok", :drug => @drug, :message => "New drug, #{@drug.name} created successfully." }
        format.json  { render :json => msg }
      else
        format.json { render json: @drug.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /drugs/1
  # PATCH/PUT /drugs/1.json
  def update
    respond_to do |format|
      if @drug.update(drug_params)
        resp = {:status => "ok", :message => "#{@drug.name} updated."}
        format.json  { render :json => resp }
      else
        format.json { render json: @drug.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /drugs/1
  # DELETE /drugs/1.json
  def destroy
    respond_to do |format|
      if @drug.destroy
        resp = {:status => "ok", :message => "Drug #{@drug.name} deleted." }
        format.json  { render :json => resp }
      else
        # format.html { redirect_to insurance_plan_path(company_id) }
        format.json { render json: @drug.errors, status: :unprocessable_entity }
      end
    end
  end
  
      #GET list of matching cheap generics
  def get_dollar_list
    
    #split up the search term into words.
    search_drugs = params[:id].scan(/\w{4,}/)
    
    pharmacies = Company.where(co_type: Company.co_types[:pharmacy]).order(:name) #this brings back ALL pharmacies
    pharmacy_ids = pharmacies.map{|pharmacy| pharmacy.id} #get pharmacy id's, this is just a poor-mans join
    drugs =  Drug.where("company_id IN (?)", pharmacy_ids) # get all the drugs that belong to a pharmacy

    @drug_map = Hash.new([])
    
    #this weeds the list down to those that match our search term. All because SQLITE cant handle "LIKE" searches on an array.
    #it was this or do multiple SQL queries. SQL Queries are slow compared to in-memory work.
    pharmacies.each do |pharmacy|
      @drug_map[{:name => pharmacy.name, :url => pharmacy.url}] = []
      drugs.each do |drug|
        if pharmacy.id == drug.company_id && search_drugs.any?{ |w| drug.name =~ /#{w}/i }
          @drug_map[{:name => pharmacy.name, :url => pharmacy.url}] += 
              [{:name => drug.name, :price => drug.price, :dose => drug.dose, :quantity => drug.quantity}]
        end
      end
    end
    respond_to do |format|
      list = render_to_string :get_dollar_list, layout: false
       msg = {:status => "ok", :list => list}
       format.json  { render :json => msg }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_drug
      @drug = Drug.where("id = ? AND company_id = ?", params[:id], params[:company_id])[0]
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def drug_params
      params.permit(:company_id, :name, :drug_class, :tier, :comments, :dose, :price, :quantity)
    end
end
