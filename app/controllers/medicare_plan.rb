class MedicarePlan < ActiveRecord::Base
    validate :advantage_plan_county
    private
    def advantage_plan_county
        if is_advantage && county.blank?
            errors.add(:base, "Advantage plans must be assigned a county")
        end
    end
end
