class AdministratorsController < ApplicationController
    before_action :require_login
    before_action :admin_user, only: :destroy
    
    def companies
        @companies = Company.where(co_type: Company.co_types[:insurance]).where.not("name LIKE ?", "%Medicare%")
        @pharmacies = Company.where(co_type: Company.co_types[:pharmacy])
    end
    
    def show
        company = Company.where(name: "#{params[:id].to_s}")[0]
        if !company
            company = Company.find(params[:id])
        end
        if !company
            redirect_to(:root)
        end
        formulary = company.drugs.order("drug_class").order("name")

        respond_to do |format|
            msg = { :status => "ok", :company => company, :formulary => formulary }
            format.json  { render :json => msg } # don't do msg.to_json
        end
    end

    private
     
      def require_login
        unless logged_in?
          flash[:danger] = "You must be logged in to access administration section"
          redirect_to signin_path # halts request cycle
        end
      end
      
    def admin_user
      redirect_to(root_url) unless current_administrator
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
      params.fetch(:company, {}).permit(:name, :url, :plan_year)
    end
end
