class CompaniesController < ApplicationController
    before_action :admin_user, only: [:add_update, :destroy]
    
    def index
        @companies = Company.where(co_type: Company.co_types[:insurance])
        @pharmacies = Company.where(co_type: Company.co_types[:pharmacy])
    end
    
    def show
        @companies = Company.select("id","name").where(co_type: Company.co_types[:insurance])
        @company = Company.find(params[:id])
        @formulary = @company.drugs
                .select("drug_class","name","tier")
                .order("drug_class")
                .order("name")
        @drug_classes = @formulary.map{|drug| drug["drug_class"]}.uniq
        pharmacy_ids = Company.select("id").where(co_type: Company.co_types[:pharmacy]).map{|pharmacy| pharmacy.id}
        @pharmacy_drugs = Drug.where("company_id IN (?)", pharmacy_ids) # this now should be a join
        render "index"
    end
    
    def add_update
        if params[:id] != "-1"
            #this is not a new record so update existing
            @company = Company.find("#{params[:id]}")
            @company.update(company_params)
        else
            #this is a new record, so make a new one
            @company = Company.create!(company_params)
        end
    
        respond_to do |format|
            if @company
                resp = {:status => "ok", :company => @company, :message => "#{@company.name} updated."}
                format.json  { render :json => resp }
            else
                format.json { render json: @company.errors, status: :unprocessable_entity }
            end
        end
    end

    def destroy
        company = Company.find("#{params[:id]}")
        respond_to do |format|
          if company.destroy
            resp = {:status => "ok", :message => "#{company.name} deleted."}
            format.json  { render :json => resp }
          else
            format.json { render json: @company.errors, status: :unprocessable_entity }
          end
        end
    end
    
    private
    
    def admin_user
      redirect_to(root_url) unless current_administrator
    end
    
    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
      params.fetch(:company, {}).permit(:name, :url, :plan_year)
    end
end
