class ContactController < ApplicationController
  def contact
  end

  def feedback
    FeedbackMailer.feedback_email(params[:session]).deliver_later
    flash[:notice] = "Email sent, thank you for the feedback!"
    redirect_to root_path
  end
end
